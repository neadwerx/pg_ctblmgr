INSERT INTO public.tb_a( foo, bar, baz )
     VALUES ( 1,2,3 ),
            ( 2,3,4 ),
            ( 3,4,5 ),
            ( 4,5,6 ),
            ( 5,6,7 ),
            ( 6,7,8 ),
            ( 7,8,9 );

INSERT INTO public.tb_b( foo, bar, baz )
     VALUES ( 1,2,3 ),
            ( 2,3,4 ),
            ( 3,4,5 ),
            ( 4,5,6 ),
            ( 5,6,7 ),
            ( 6,7,8 ),
            ( 7,8,9 );

INSERT INTO public.tb_c( foo, bar, baz )
     VALUES ( 1,2,3 ),
            ( 2,3,4 ),
            ( 3,4,5 ),
            ( 4,5,6 ),
            ( 5,6,7 ),
            ( 6,7,8 ),
            ( 7,8,9 );

INSERT INTO public.tb_d( foo, bar, baz )
     VALUES ( 1,2,3 ),
            ( 2,3,4 ),
            ( 3,4,5 ),
            ( 4,5,6 ),
            ( 5,6,7 ),
            ( 6,7,8 ),
            ( 7,8,9 );

INSERT INTO public.tb_e( foo, bar, baz )
     VALUES ( 1,2,3 ),
            ( 2,3,4 ),
            ( 3,4,5 ),
            ( 4,5,6 ),
            ( 5,6,7 ),
            ( 6,7,8 ),
            ( 7,8,9 );

INSERT INTO public.tb_f( foo, bar, baz )
     VALUES ( 1,2,3 ),
            ( 2,3,4 ),
            ( 3,4,5 ),
            ( 4,5,6 ),
            ( 5,6,7 ),
            ( 6,7,8 ),
            ( 7,8,9 );
